package fr.greta.formation.dglv.temperature.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controler.TemperatureControleur;
import controler.ThreadAppelWebService;
import modele.EvenementChangementDeTemperature;
import modele.IAbonneTemperature;

public class JFrameFieldTemperature implements ActionListener, IAbonneTemperature {
	
	private JFrame mFrame = null;
	private JPanel mContentPane = null;
	private JButton mButton = null;
	private JFormattedTextField mField;
	private JButton mButtonIncremente = null;
	private TemperatureControleur tc = null;
	
	public JFrameFieldTemperature(int tempInit,TemperatureControleur tempcont)  {
		
		tc = tempcont;
		
		mFrame = new JFrame("Temperature");
		mFrame.setSize(300, 100);
		
		mContentPane = new JPanel(); 

		mField = new JFormattedTextField(tempInit);
		mContentPane.add(mField); 
		mField.setPreferredSize(new Dimension (50, 30));

		
		mButton = new JButton("Mettre à jour");
		mContentPane.add(mButton); // Ajoute le bouton 
		
		mButtonIncremente = new JButton("+");
		mContentPane.add(mButtonIncremente); // Ajoute le bouton 
		mButtonIncremente.addActionListener(this);
		
		
		mFrame.add(mContentPane);
		mButton.addActionListener(this);
		
	}
	
	public void affiche() {
		mFrame.setVisible(true);
	}
	
	

	public void actionPerformed(ActionEvent e) {
		
		ThreadAppelWebService test = new ThreadAppelWebService();
		test.start();
		try {
			test.join(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		test.interrupt();
		tc.changerLaTemperature(ThreadAppelWebService.tempHasard);
		
		if(e.getSource()== mButton) 
		{
			//int tempo=(Integer) mField.getValue();
			int tempo = ThreadAppelWebService.tempHasard;
			//System.out.println(mField.getValue().toString());
			tc.changerLaTemperature(tempo);
		}
		
		if(e.getSource()== mButtonIncremente) 
		{
			int tempo=(Integer) mField.getValue();
			//mField.setValue(tempo+1);
			tc.changerLaTemperature(tempo+1);
		}
		
	}

	public void temperatureChangee(EvenementChangementDeTemperature ecdt) {
		// TODO Auto-generated method stub
		mField.setValue(ecdt.getNewTemperature());
	}
	
	

}

