package fr.greta.formation.dglv.temperature.main;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.EvenementChangementDeTemperature;
import modele.IAbonneTemperature;

public class JFrameLabelTemperature implements IAbonneTemperature{
	
	private JFrame mFrame = null;
	private JPanel mContentPane = null;
	private JLabel mLabel = null;

	
	public JFrameLabelTemperature(int pTemperature){
		construitFrame(pTemperature);
	}
	
	private void construitFrame(int temperature) {
		mFrame = new JFrame();
		
		mContentPane = new JPanel();
		
		mLabel = new JLabel();
		mLabel.setText(Integer.toString(temperature));
		mContentPane.add(mLabel);
		
	
		mFrame.setContentPane(mContentPane);
		mFrame.setTitle("JFrameFieldTemperature");
		mFrame.pack();
	}
	public void affiche() {
		mFrame.setVisible(true);
	}
	public void termine() {
		mFrame.dispose();
	}

	public void temperatureChangee(EvenementChangementDeTemperature ecdt) {
		mLabel.setText(Integer.toString(ecdt.getNewTemperature()));
	}
}

