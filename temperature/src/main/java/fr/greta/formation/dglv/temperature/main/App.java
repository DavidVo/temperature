package fr.greta.formation.dglv.temperature.main;

import controler.TemperatureControleur;
import modele.TemperatureModele;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
    	System.out.println( "Début!" );
    	TemperatureModele lTM = new TemperatureModele();
    	TemperatureControleur lTC = new TemperatureControleur(lTM);
    	System.out.println( "Fin!" );

    }
}
