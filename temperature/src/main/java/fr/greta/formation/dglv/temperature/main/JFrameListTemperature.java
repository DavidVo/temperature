package fr.greta.formation.dglv.temperature.main;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import modele.EvenementChangementDeTemperature;
import modele.IAbonneTemperature;

public class JFrameListTemperature implements IAbonneTemperature{
	private JFrame mFrame = null;
	private JPanel mContentPane = null;
	
	private JList<Integer> mListTemperature = null;
	private DefaultListModel<Integer> mListModel = null; // tiens, tiens, un modèle...
	
	private JScrollPane mScrollTemperature = null;
	
	public JFrameListTemperature(int ltemp){
		construitFrame(ltemp);
	}
	
	private void construitFrame(int temp) {
		mFrame = new JFrame();
		mContentPane = new JPanel();
		
		mListModel = new DefaultListModel<Integer>();
		mListTemperature = new JList<Integer>(mListModel);
		
		mScrollTemperature = new JScrollPane(mListTemperature);
		mListModel.addElement((Integer) temp);
		
		mContentPane.add(mScrollTemperature);
		
		mFrame.setContentPane(mContentPane);
		mFrame.setTitle("JFrameListTemperature");
		
	}
	
	public void affiche() {
		mFrame.setVisible(true);
	}
	
	public void termine() {
		mFrame.dispose();
	}
	
	
	public void temperatureChangee(EvenementChangementDeTemperature ecdt) {
		// TODO Auto-generated method stub
		mListModel.addElement(ecdt.getNewTemperature());
	}
}
