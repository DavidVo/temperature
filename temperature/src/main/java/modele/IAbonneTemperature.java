package modele;

import java.util.EventListener;

public interface IAbonneTemperature extends EventListener {
	
	void temperatureChangee(EvenementChangementDeTemperature ecdt);
}
