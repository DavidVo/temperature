package modele;

import javax.swing.event.EventListenerList;


public class TemperatureModele {
	
	private int mTemperature;
	
	private EventListenerList mListeDesAbonnes = new EventListenerList();
	
	public void ajouteAbonne(IAbonneTemperature pAbonne) {
		mListeDesAbonnes.add(IAbonneTemperature.class, pAbonne);
	}
	
	public void supprimeAbonne(IAbonneTemperature pAbonne) {
		mListeDesAbonnes.remove(IAbonneTemperature.class, pAbonne);
	}
	
	public void notifierChangementDeTemperature() {
		IAbonneTemperature[] eachAbonnee =  mListeDesAbonnes.getListeners(IAbonneTemperature.class);
		for(int i = 0; i < eachAbonnee.length; i++ ) {
	        eachAbonnee[i].temperatureChangee(new EvenementChangementDeTemperature(this, mTemperature));
		}
	}

	public int getmTemperature() {
		return mTemperature;	
	}

	public void setmTemperature(int mTemperature) {
		this.mTemperature = mTemperature;
		notifierChangementDeTemperature();
		
	}
	

	
	
}