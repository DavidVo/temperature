package modele;

import java.util.EventObject;

public class EvenementChangementDeTemperature extends EventObject{
	private static final long serialVersionUID = 1L;
	private int mNouvelleTemperature;
	public EvenementChangementDeTemperature(Object source, int pNouvelleTemperature){
		super(source);
		
		this.mNouvelleTemperature = pNouvelleTemperature;
	}
	public int getNewTemperature(){
		return mNouvelleTemperature;
	}
}

