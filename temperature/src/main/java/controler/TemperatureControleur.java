package controler;

import fr.greta.formation.dglv.temperature.main.JFrameFieldTemperature;
import fr.greta.formation.dglv.temperature.main.JFrameLabelTemperature;
import fr.greta.formation.dglv.temperature.main.JFrameListTemperature;
import modele.EvenementChangementDeTemperature;
import modele.IAbonneTemperature;
import modele.TemperatureModele;

public class TemperatureControleur {
	
	public JFrameFieldTemperature mFieldView;
	public JFrameLabelTemperature mLabelView;
	public JFrameListTemperature mListView;
	private TemperatureModele mModele;
	
	public TemperatureControleur(TemperatureModele lTM) {
		
		this.mModele = lTM;
		
		mFieldView = new JFrameFieldTemperature(0, this);
		lTM.ajouteAbonne(mFieldView);
		mFieldView.affiche();
		
		
		mLabelView = new JFrameLabelTemperature(mModele.getmTemperature());
		lTM.ajouteAbonne(mLabelView);
		mLabelView.affiche();
		
		mListView = new JFrameListTemperature(0);
		lTM.ajouteAbonne(mListView);
		mListView.affiche();
		
		IAbonneTemperature lVue = new IAbonneTemperature(){
			public void temperatureChangee(EvenementChangementDeTemperature event) {
				System.out.println(event.getNewTemperature());	
			}}; 
			
			lTM.ajouteAbonne(lVue);
		
	}
	
	public void changerLaTemperature(int pTemperature) {
		mModele.setmTemperature(pTemperature);
	}
	


}
