package controler;

import java.util.Random;

public class ThreadAppelWebService extends Thread {
	
	private static int mNumeroDeThread = 0;
	public static int tempHasard = 0;
	
	@Override
	public void run() {
		mNumeroDeThread+=1;
		try {
			// fait une action lente
			for (int i=5; i>0;i--)
			{
				this.setName("Thread_" + mNumeroDeThread);
				System.out.println(this.getName() + " "+ i);				
				Thread.sleep(1000);

				
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		tempHasard = getUnEntierAuHasardEntre(1, 50);
		System.out.println(tempHasard);
	}
	
	private static int getUnEntierAuHasardEntre(int pMin, int pMax) {
		if (pMin >= pMax) {
			throw new IllegalArgumentException("max doit être plus grand que min");
		}
		Random lR = new Random();
		return lR.nextInt((pMax - pMin) + 1) + pMin;
	}
	
	

	
}
