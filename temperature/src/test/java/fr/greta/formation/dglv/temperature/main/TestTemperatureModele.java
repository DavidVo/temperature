package fr.greta.formation.dglv.temperature.main;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*; 
import static org.junit.Assert.*;

import modele.EvenementChangementDeTemperature;
import modele.IAbonneTemperature;
import modele.TemperatureModele;

public class TestTemperatureModele {
	
	boolean mLaTemperatureAChange = false;
	int mNbNotificationVue1;
	int mNbVuesNotifiees;
	
	@Before
	public void setup () {
		mNbNotificationVue1=0;
		mNbVuesNotifiees=0;	
	}
	
	@Test
	public void uneValeurEstBienStockee() {
		final int lValeur = 12;
		TemperatureModele lTM = new TemperatureModele();
		lTM.setmTemperature(lValeur);
		assertThat(lValeur, is(lTM.getmTemperature()));

	}

	@Test
	public void laModificationDeLaValeurEstNotifieeAUneVue() {
		final int lValeur = 12;
		TemperatureModele lTM = new TemperatureModele();
		lTM.setmTemperature(lValeur);
		lTM.ajouteAbonne(new IAbonneTemperature() {
				public void temperatureChangee(EvenementChangementDeTemperature event) {
					mLaTemperatureAChange = true;
				}
		});
		
		System.out.println();
		assertThat(mLaTemperatureAChange, is(false));
		lTM.setmTemperature(13);
		assertThat(mLaTemperatureAChange, is(true));
	}
	
	@Test
	public void laModificationDeLaValeurNEstPasNotifieeAUneVueAbonnePuisDesabonnee() {
		final int lValeur = 12;
		TemperatureModele lTM = new TemperatureModele();
		lTM.setmTemperature(lValeur);
		
		IAbonneTemperature lVue = new IAbonneTemperature(){
			public void temperatureChangee(EvenementChangementDeTemperature event) {
				mLaTemperatureAChange = true;
			}}; 
			
		lTM.ajouteAbonne(lVue);
		lTM.supprimeAbonne(lVue);
		lTM.setmTemperature(13);
		
		assertThat(mLaTemperatureAChange, is(false));
		
	}
	
	@Test
	public void laModificationDeLaValeurEstNotifieeDeuxFoisAUneMemeVue() {
		final int lValeur = 12;
		TemperatureModele lTM = new TemperatureModele();
		lTM.setmTemperature(lValeur);
		
		IAbonneTemperature lVue = new IAbonneTemperature(){
			public void temperatureChangee(EvenementChangementDeTemperature event) {
				mNbNotificationVue1 += 1;
				mLaTemperatureAChange = true;
				
			}}; 
			
		lTM.ajouteAbonne(lVue);
		lTM.setmTemperature(13);
		lTM.setmTemperature(14);
		
		assertThat(2, is(mNbNotificationVue1));	

	}
	
	@Test
	public void laModificationDeLaValeurEstNotifieeADeuxVuesDifferentes() {
		final int lValeur = 12;
		TemperatureModele lTM = new TemperatureModele();
		lTM.setmTemperature(lValeur);
		
		IAbonneTemperature lVue = new IAbonneTemperature(){
			public void temperatureChangee(EvenementChangementDeTemperature event) {
				mNbVuesNotifiees += 1;
				mLaTemperatureAChange = true;
				
			}}; 
			
		IAbonneTemperature lPue = new IAbonneTemperature(){
			public void temperatureChangee(EvenementChangementDeTemperature event) {
				mNbVuesNotifiees += 1;
				mLaTemperatureAChange = true;
					
			}}; 
			
		lTM.ajouteAbonne(lVue);
		lTM.ajouteAbonne(lPue);
		lTM.setmTemperature(13);
		
		assertThat(2, is(mNbVuesNotifiees));	
		
	}
	
	}

